# Use official Node's alpine image
FROM node:9.11.1-alpine

# Create WEB_PATH it doesn't exist and set it as working dir
ENV WEB_PATH=/usr/src/web
WORKDIR ${WEB_PATH}

# Define base envronment variables for Web
ENV HOST=0.0.0.0 \
    PORT=3000

# Expose default port to connect with the service
EXPOSE ${PORT}

# Copy package.json and yarn.lock
COPY package.json yarn.lock ./

# Install dependencies
RUN yarn install

# Copy application code to the build path
COPY . .

# Define default command to execute when running the container
CMD ["yarn", "run", "dev"]
