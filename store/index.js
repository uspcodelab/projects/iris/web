import Vuex from "vuex";

const createStore = () => {
  return new Vuex.Store({
    state: {
      isLoggedIn: false,
      user: {
        id: null,
        name: null,
        email: null,
        token: null
      }
    },
    mutations: {
      setUser(state, data) {
        state.user.token = data.Authorization;
        state.user.id = data.user.id;
        state.user.name = data.user.name;
        state.user.email = data.user.email;
        state.isLoggedIn = true;
      },
      clearUser(state) {
        state.user.token = null;
        state.user.id = null;
        state.user.name = null;
        state.user.email = null;
        state.isLoggedIn = false;
      }
    },
    actions: {
      authenticateUser(vuexContext, authData) {
        return this.$axios
          .$post("auth/login", authData)
          .then(response => {
            console.log(response);
            vuexContext.commit("setUser", response);
            localStorage.setItem("token", vuexContext.state.user.token);
            localStorage.setItem("name", vuexContext.state.user.name);
            localStorage.setItem("email", vuexContext.state.user.email);
            localStorage.setItem("id", vuexContext.state.user.id);
            vuexContext.dispatch("setLogoutTimer");
          })
          .catch(e => {
            console.log(e);
            throw new Error(e);
          });
      },
      setLogoutTimer(vuexContext) {
        setTimeout(() => {
          vuexContext.commit("clearToken");
        }, 36005 * 1000);
      },
      initAuth(vuexContext) {
        const token = localStorage.getItem("token");
        const name = localStorage.getItem("name");
        const email = localStorage.getItem("email");
        const id = localStorage.getItem("id");
        if (token) {
          let data = {
            Authorization: token,
            user: {
              name,
              email,
              id
            }
          };
          vuexContext.commit("setUser", data);
          return true;
        }
        return false;
      },
      logout(vuexContext) {
        return this.$axios
          .post("/auth/logout", {
            Authorization: vuexContext.state.user.token
          })
          .then(result => {
            console.log(result);
            vuexContext.commit("clearUser");
            localStorage.removeItem("token");
            localStorage.removeItem("name");
            localStorage.removeItem("email");
            localStorage.removeItem("id");
          })
          .catch(e => {
            console.log(e.response.data.message);
            vuexContext.commit("clearUser");
            localStorage.removeItem("token");
            localStorage.removeItem("name");
            localStorage.removeItem("email");
            localStorage.removeItem("id");
            this.$router.push("/login");
            throw e;
          });
      }
    },
    getters: {
      isAuthenticated(state) {
        return state.user.token != null;
      }
    },
    getters: {
      isAuthenticated(state) {
        return state.user.token !== null;
      },
      getUser(state) {
        return state.user;
      }
    }
  });
};

export default createStore;
